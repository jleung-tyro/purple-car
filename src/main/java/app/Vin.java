package app;

class Vin {

    private static final String VIN_FORMAT_REGEX = "^[a-zA-Z0-9]*$";
    private final String vin;

    Vin(String vin) throws InvalidCarException {
        validateNotNull(vin);
        validateLength(vin);
        validateAlphanumeric(vin);
        validateFormat(vin);
        this.vin = vin;
    }

    private void validateFormat(String vin) throws InvalidCarException {
        if (!vin.matches(VIN_FORMAT_REGEX)) {
            throw new InvalidCarException(5, "Vin does not match format");
        }
    }

    private void validateAlphanumeric(String vin) throws InvalidCarException {
        if (!vin.matches("[a-z0-9]*")) throw new InvalidCarException(997, "Not alphanumeric");
    }

    private void validateLength(String vin) throws InvalidCarException {
        if (vin.length() != 17) {
            throw new InvalidCarException(4, "Vin has incorrect length.");
        }
    }

    private void validateNotNull(String vin) throws InvalidCarException {
        if (vin == null) {
            throw new InvalidCarException(6, "Null vin");
        }
    }
}
