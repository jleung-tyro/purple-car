package app;

public class Car {

    private final CarId carID;
    private final Vin vin;
    private final CarModel model;
    //private String plateNo;
    //private File photo;

    public Car(Integer carID, String vin, String model) throws InvalidCarException {
        this.carID = new CarId(carID);
        this.vin = new Vin(vin);
        this.model = new CarModel(model);
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.model;
    }

}
