package app.encryption;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class FileDecryptService {
    private static final String ALGORITHM = "AES";
    private Cipher cipher;

    public FileDecryptService(Cipher cipher) {
        this.cipher = cipher;
    }

    public String decrypt(SecretKey key, byte[] text) throws Exception {
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getKey(), ALGORITHM));
        return new String(cipher.doFinal(text));
    }
}
