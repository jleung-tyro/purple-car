package app.encryption;

import java.util.Arrays;

public class SecretKey {
    private byte[] key;
    private boolean consumed;

    public SecretKey(byte[] key) {
        this.key = key;
    }

    byte[] getKey() {
        if (consumed) {
            throw new RuntimeException("Key already consumed");
        }
        consumed = true;
        byte[] clonedKey = key.clone();
        Arrays.fill(key, (byte) '0');
        return clonedKey;
    }
}
