package app;

class CarId {
    private final Integer id;

    CarId(Integer id) throws InvalidCarException {
        validate(id);
        this.id = id;
    }

    private void validate(Integer id) throws InvalidCarException {
        if (id == null || id < 0) {
            throw new InvalidCarException(995, "Invalid Card ID");
        }
    }

}
