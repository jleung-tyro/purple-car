package app;

class CarModel {
    private final String model;

    CarModel(String model) throws InvalidCarException {
        validateBetweenOneLessThanTwentyChar(model);
        validateAlphanumeric(model);
        validateNotNull(model);
        this.model = model;
    }

    private void validateAlphanumeric(String model) throws InvalidCarException {
        if (!model.matches("[a-z0-9]*")) throw new InvalidCarException(997, "Not alphanumeric");
    }

    private void validateNotNull(String model) throws InvalidCarException {
        if  (model == null) throw new InvalidCarException(998, "Car model null");
    }

    private void validateBetweenOneLessThanTwentyChar(String model) throws InvalidCarException {
        if (model.length() < 2 || model.length() > 20) {
            throw new InvalidCarException(999, "Car model length not between 2 and 20");
        }
    }
}
