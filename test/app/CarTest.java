package app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class CarTest {

    private static final String VALID_VIN = "12345678901234567";
    private static final String VALID_MODEL = "model";

    @Test
    void shouldThrowExceptionGivenCarIdIsNull() {
        assertThrows(InvalidCarException.class,  () -> new Car(null,VALID_VIN, VALID_MODEL));
    }


    @Test
    void shouldThrowExceptionGivenVinIsNull() {
        assertThrows(InvalidCarException.class,  () -> new Car(1,null, VALID_MODEL));
    }

    @Test
    void shouldThrowExceptionGivenVinTooLong() {
        assertThrows(InvalidCarException.class,  () -> new Car(1,"12345678901234567890",VALID_MODEL));
    }

    @Test
    void shouldThrowExceptionGivenVinNotAlphanumeric() {
        assertThrows(InvalidCarException.class,  () -> new Car(1,"@",VALID_MODEL));
    }

    @Test
    void shouldThrowExceptionGivenModelTooLong() {
        assertThrows(InvalidCarException.class,  () -> new Car(1, VALID_VIN,"modelmodelmodelmodelmodel"));
    }

    @Test
    void shouldThrowExceptionGivenModelTooShort() {
        assertThrows(InvalidCarException.class,  () -> new Car(1,VALID_VIN,"1"));
    }

    @Test
    void shouldThrowExceptionGivenModelIsNull() {
        assertThrows(NullPointerException.class,  () -> new Car(1,VALID_VIN,null));
    }

}
