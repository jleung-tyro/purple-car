package app;

import app.encryption.FileDecryptService;
import app.encryption.SecretKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DecryptTest {
    private FileDecryptService fileDecryptService;

    @BeforeEach
    void setUp() throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        fileDecryptService = new FileDecryptService(cipher);
    }

    @Test
    void shouldDecryptUsingSecretKey() throws Exception {
        String textToEncrypt = "Some text";
        byte[] encryptedText = encryptText(textToEncrypt);
        String decryptedText = decryptText(encryptedText);
        assertEquals(textToEncrypt, decryptedText);
        assertThrows(RuntimeException.class, () -> decryptText(encryptedText));
    }

    private String decryptText(byte[] encryptedText) throws Exception {
        return fileDecryptService.decrypt(new SecretKey(System.getenv("KEY").getBytes()), encryptedText);
    }

    private byte[] encryptText(String text) throws Exception {
        String key = System.getenv("KEY");
        Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        return cipher.doFinal(text.getBytes());
    }
}
